const express = require("express");
const bodyParser = require("body-parser");
const logger = require("morgan");
const indexRouter = require("./routes/index");
const authRouter = require("./routes/auth");
const { register } = require("./event/register");
const app = express();
var cors = require("cors");

// Add middlewares in order of execution
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Configure CORS
app.options("*", cors());

/**
 * Update these origins later
 */
var allowedOrigins = ["*"];
app.use(
  cors({
    origin: function (origin, callback) {
      // allow requests with no origin
      // (like mobile apps or curl requests)
      if (!origin) return callback(null, true);
      /*if(allowedOrigins.indexOf(origin) === -1){
          var msg = 'The CORS policy for this site does not ' +
                    'allow access from the specified Origin.';
          return callback(new Error(msg), false);
        }*/
      return callback(null, true);
    },
  })
);

// Mount routes
app.use("/", indexRouter);
app.use("/auth", authRouter);

if (global.UnitTest == undefined) {
  register();
}

module.exports = app;
