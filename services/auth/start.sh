if [ "$NODE_ENV" = 'development' ]; then
    node /app/node_modules/nodemon/bin/nodemon.js --inspect=0.0.0.0:1234 ./bin/www
else
    node ./bin/www
fi
