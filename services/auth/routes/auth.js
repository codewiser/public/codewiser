const { graphql } = require("../utils/graphql.js");
const { config } = require("../utils/config.js");
const fs = require("fs");
const express = require("express");
const router = express.Router();
const passwordHash = require("password-hash");
const TokenGenerator = require("../utils/token-generator");
const privateKey = fs.readFileSync(config.key_pair_path + "/key", "utf8");
const publicKey = fs.readFileSync(config.key_pair_path + "/key.pem", "utf8");

const tokenOptions = {
  algorithm: "RS256",
  expiresIn: 60 * 60, //In Seconds
  issuer: config.issuer,
  audience: config.audience,
  jwtid: "1",
};
const tokenGenerator = new TokenGenerator(privateKey, publicKey, tokenOptions);

const STATUS_SUCCESS = "OK";
const STATUS_USER_NOT_FOUND = "User Not Found";
const STATUS_DB_UPDATE_ERROR = "Error Updating Database";
const STATUS_INVALID_INPUT = "Invalid Input";
const STATUS_INVALID_OTP = "Invalid OTP";
const STATUS_INVALID_PASSWORD = "Invalid Password";
const STATUS_SIGNUP_ERROR = "Signup Error";

router.post("/get-guest-token", async function (req, res, next) {
  const options = {
    subject: "guest",
  };

  roles = ["guest"];
  const payload = {
    roles: roles,
    "https://codewiser.in/claims": {
      "x-hasura-allowed-roles": roles,
      "x-hasura-default-role": "guest",
      "x-hasura-user-id": "0",
    },
  };

  token = {
    token: tokenGenerator.sign(payload, options),
    publicKey: publicKey,
  };

  res.send(token);
});

router.post("/signup", async function (req, res, next) {
  let ret = {status: STATUS_SUCCESS, id: 0};
  const mutation = `mutation insert_auth_user($object: [auth_user_insert_input!]!){
    insert_auth_user(objects: $object){
      returning{
        id
      }
    }
  }
  `;
  try {
    result = await graphql.query(mutation, { object: {
      first_name: req.body.firstname,
      last_name: req.body.lastname,
      email: req.body.email,
      mobile: req.body.mobile, 
      password_hash: passwordHash.generate(req.body.password),
      service_accesses: {
        data: {
          role_id: 1,
          service_id: 1
        }
      }
    }});
    ret.id = result.insert_auth_user.returning[0].id;
  } catch (error) {
    console.log(error);
    if(error.message == "GraphQL Error: Uniqueness violation. duplicate key value violates unique constraint \"user_mobile_key\""){
      ret.status = "Mobile number already registered, please try different one";
    }
    else {
      ret.status = error.message;
    }
  }
  res.send(ret);
});

router.post("/get-token", async function (req, res, next) {
  let result = null;
  const query = `query
      auth_user($username: String){
        auth_user(where: {mobile: {_eq: $username}}) {
          id,
          first_name,
          last_name,
          email,
          mobile,
          password_hash,
          user_status_id,
          service_accesses{
            service{
              name,
              display_name
            },
            role{
              role
            },
          }
        }
      }
    `;

  try {
    result = await graphql.query(query, { username: req.body.username });
  } catch (error) {
    console.log(error);
  }

  token = { token: null, status: STATUS_USER_NOT_FOUND };
  if (result.auth_user && result.auth_user.length > 0) {
    if (2 == result.auth_user[0].user_status_id) {
      token.status = STATUS_USER_NOT_FOUND;
    } 
    else if (false == verifyPassword(req.body.password, result.auth_user[0].password_hash)) {
      token.status = STATUS_INVALID_PASSWORD;
    } else {
      delete result.auth_user[0].password_hash;
      token.token = generateToken(result.auth_user[0]);
      token.public_key = publicKey;
      token.status = STATUS_SUCCESS;
    }
  }
  res.send(token);
});

router.post("/refresh-token", async function (req, res, next) {
  let result = null;
  const payload = tokenGenerator.decode(req.body.token);
  const query = `query
    auth_user($id: bigint){
      auth_user(where: {id: {_eq: $id}}) {
        id,
        first_name,
        last_name,
        email,
        mobile,
        user_status_id,
        service_accesses{
          service{
            name,
            display_name
          },
          role{
            role
          },
        }
      }
    }
    `;

  token = { token: null, status: STATUS_USER_NOT_FOUND };
  if (payload != null) {
    try {
      result = await graphql.query(query, { id: payload.user_id });
    } catch (error) {
      console.log(error);
    }

    if (result.auth_user && result.auth_user.length > 0) {
      if (2 == result.auth_user[0].user_status_id) {
        token.status = STATUS_USER_NOT_FOUND;
      } 
      else {
        token.status = STATUS_SUCCESS;
        token.token = generateToken(result.auth_user[0]);
      }
    }
  }
  res.send(token);
});

router.post("/request-reset-password", async function (req, res, next) {
  let result = null;
  const query = `query
    auth_user($username: String){
      auth_user(where: {mobile: {_eq: $username}}) {
        email,
        mobile,
        user_status_id
      }
    }
  `;

  const mutation = `mutation update_auth_user($username: String, $otp: Int){
    update_auth_user(
      where: {mobile: {_eq: $username}}
      _set: {password_otp: $otp}
    ){
      returning{
        password_otp
      }
    }
  }`;

  status = STATUS_USER_NOT_FOUND;
  try {
    result = await graphql.query(query, { username: req.body.username });
  } catch (error) {
    console.log(error);
  }
  if (result.auth_user && result.auth_user.length === 1) {
    otp = generateOtp();
    try {
      resultMuatation = await graphql.query(mutation, {
        username: req.body.username,
        otp: otp,
      });
      status = STATUS_SUCCESS;
      sendOtp(result.auth_user[0].mobile, result.auth_user[0].email, otp);
    } catch (error) {
      status = STATUS_DB_UPDATE_ERROR;
      console.log(error);
    }
  }

  res.send({ status: status });
});

router.post("/resend-password-otp", async function (req, res, next) {
  let result = null;
  const query = `query
    auth_user($username: String){
      auth_user(where: {mobile: {_eq: $username}}) {
        email,
        mobile,
        password_otp,
        user_status_id
      }
    }
  `;

  status = STATUS_USER_NOT_FOUND;
  try {
    result = await graphql.query(query, { username: req.body.username });
  } catch (error) {
    console.log(error);
  }
  if (result.auth_user.length === 1) {
    sendOtp(
      result.auth_user[0].mobile,
      result.auth_user[0].email,
      result.auth_user[0].password_otp
    );
    status = STATUS_SUCCESS;
  }
  res.send({ status: status });
});

router.post("/reset-password", async function (req, res, next) {
  let result = null;
  const query = `query
    auth_user($username: String){
      auth_user(where: {mobile: {_eq: $username}}) {
		    id
        password_otp,
        password_hash
      }
    }
  `;

  const mutation = `mutation update_auth_user($username: String, $password: String){
    update_auth_user(
      where: {mobile: {_eq: $username}}
      _set: {password_otp: null, password_hash: $password}
    ){
      returning{
        id
      }
    }
  }`;

  status = STATUS_USER_NOT_FOUND;
  try {
    result = await graphql.query(query, { username: req.body.username });
  } catch (error) {
    console.log(error);
  }
  if (result.auth_user.length === 1) {
    status = STATUS_INVALID_INPUT;
    if (req.body.otp != undefined && req.body.otp != result.auth_user[0].password_otp) {
      status = STATUS_INVALID_OTP;
    } 
    else if (req.body.old_password != undefined && false == verifyPassword(req.body.old_password, result.auth_user[0].password_hash)) {
      status = STATUS_INVALID_PASSWORD;
    } else if (req.body.otp != undefined || req.body.old_password != undefined) {
      try {
        result = await graphql.query(mutation, {
                username: req.body.username,
                password: passwordHash.generate(req.body.new_password),
              });
        status = STATUS_SUCCESS;
      } catch (error) {
        status = STATUS_DB_UPDATE_ERROR;
        console.log(error);
      }
    }
  }
  res.send({ status: status });
});

function generateOtp() {
  var otp = "";
  if (process.env.SEND_SMS == "true") {
    var digits = "1234567899";
    for (let i = 0; i < 4; i++) {
      otp += digits[Math.floor(Math.random() * 10)];
    }
  } else {
    otp = "1234";
  }
  return parseInt(otp);
}

function sendOtp(mobile, email, otp) {
  const msg = "<%23> Your OTP is :" + otp;
  //sendSmsMessage(mobile, msg); //integrate this with SMS service provider
}


function verifyPassword(password, password_hash) {
  if (password != undefined && password != "" && password_hash != "") {
    return passwordHash.verify(password, password_hash);
  }
  return false;
}

function generateToken(user) {
  const options = {
    subject: user.mobile,
  };

  let roles = [];
  let hasura_roles = [];

  user.service_accesses.forEach((element) => {
    roles.push({
      service: element.service.name,
      role: element.role.role,
      display_name: element.service.display_name,
    });
    if (hasura_roles.indexOf(element.role.role) == -1) {
      hasura_roles.push(element.role.role);
    }
  });

  const payload = {
    first_name: user.first_name,
    last_name: user.last_name,
    mobile: user.mobile,
    email: user.email,
    user_id: user.id,
    roles: roles,
    "https://codewiser.in/claims": {
      "x-hasura-allowed-roles": hasura_roles,
      "x-hasura-default-role": "user",
      "x-hasura-user-id": user.id.toString(),
    },
  };
  return tokenGenerator.sign(payload, options);
}

module.exports = router;
