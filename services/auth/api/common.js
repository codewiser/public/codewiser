const { graphql } = require("../utils/graphql.js");

async function queryEntity(schema,table,queryFields,where) {
    const query = `query ${schema}_${table} ($where: ${schema}_${table}_bool_exp!){
        ${schema}_${table}(where: $where){
            ${queryFields}
        }
    }
    `;
    try {
        result = await graphql.query(query, {where: where});
        return result;
    } catch (error) {
        console.log("################## Error: Query Failed ##################");
        console.log(error.message);
        return null;
    }
}

module.exports = {
    queryEntity,
};
