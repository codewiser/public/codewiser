const {removeTriggers, triggers} =  require('./triggers');
const { headers } =  require('../utils/graphql');
const { config } = require('../utils/config');
const axios = require("axios");

async function waitForGraphQL(){
    while(true){
        try {
            response = await axios.get(config.graphql_endpoint+"/v1/version");
            console.log("GraphQL Ready...")
            return;
        }
        catch(err){
            console.log("Waiting for GraphQL...")
        };
    }
}

async function unRegisterEventTrigger(query, done){
    try {
        response = await axios.post(config.graphql_endpoint+"/v1/query",
                                    {"type": "delete_event_trigger", "args": {"name": query.name}},
                                    {headers: headers}
                                    );
        done();
    }
    catch(err){
        console.log("info: delete_event_trigger failed for "+query.name+". It may not exist")
    };
}

async function registerEventTrigger(query, done){
    try {
        response = await axios.post(config.graphql_endpoint+"/v1/query",
                                    {"type": "create_event_trigger", "args": query},
                                    {headers: headers}
                                    );
        if(response.data.message != 'success'){
            console.log(response.data);
        }
        done();
    }
    catch(err){
        console.log(err.message);
    };
}


async function registerService(name){
    try {
        response = await axios.post(config.graphql_endpoint+"/v1/query",
                                    {"type": "remove_remote_schema", "args": {"name": name}},
                                    {headers: headers}
                                    );
    }
    catch(err){
        console.log("info: remove_remote_schema failed for "+name + "err : "+err);
    };
    try {
        response = await axios.post(config.graphql_endpoint+"/v1/query",
                                    {"type": "add_remote_schema", "args": {
                                        "name": name,
                                        "definition": {
                                            "url": "http://" + name + ":8080/graphql",
                                            "forward_client_headers": true,
                                            "timeout_seconds": 60
                                        },
                                        "headers": [],
                                        "comment": name + " APIs"
                                    }},
                                    {headers: headers}
                                    );
    }
    catch(err){
        console.log("Error: add_remote_schema failed for "+name + "err : "+err);
    };
    try {
        response = await axios.post(config.graphql_endpoint+"/v1/query",
                                    {"type": "reload_remote_schema", "args": {"name": name}},
                                    {headers: headers}
                                    );
    }
    catch(err){
        console.log("Error: reload_remote_schema failed for "+name + "err : "+err);
    };
    return true;
}

async function registerTriggers(){
    removeTriggers.forEach(element => {
        unRegisterEventTrigger(element, function(){
            console.log('Un Registered event trigger: '+element.name);
        });
    });

    triggers.forEach(element => {
        registerEventTrigger(element, function(){
            console.log('Registered event trigger: '+element.name);
        });
    });
}

async function register(){
    await waitForGraphQL();
    //await registerService("cyclone");
    await registerTriggers(); 
}


module.exports = {register};
