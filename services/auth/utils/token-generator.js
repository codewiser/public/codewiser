const jwt = require('jsonwebtoken');

function TokenGenerator (privateKey, publicKey, options) {
  this.privateKey = privateKey;
  this.publicKey = publicKey;
  this.options = options; //algorithm + keyid + noTimestamp + expiresIn + notBefore
}

TokenGenerator.prototype.sign = function(payload, signOptions) {
  const jwtSignOptions = Object.assign({}, signOptions, this.options);
  return jwt.sign(payload, this.privateKey, jwtSignOptions);
}

// verifyOptions = options you would use with verify function
TokenGenerator.prototype.refresh = function(token, verifyOptions) {
  // The first signing converted all needed options into claims, they are already in the payload
  const payload = jwt.decode(token, verifyOptions);
  const jwtSignOptions = Object.assign({ }, this.options, { jwtid: (parseInt(payload.jti) + 1).toString()});
  delete payload.iat;
  delete payload.exp;
  delete payload.nbf;
  delete payload.jti;
  delete payload.aud;
  delete payload.iss;
  return jwt.sign(payload, this.privateKey, jwtSignOptions);
}

TokenGenerator.prototype.decode = function(token) {
  return jwt.decode(token);
}

module.exports = TokenGenerator;
