const IST_TIME_ZONE_OFFSET = -330; //+5.30 Hrs
const getUtcTime = (time) => {
    return new Date(time.getTime() + (IST_TIME_ZONE_OFFSET * 60000));
}

const getLocalTime = (time) => {
    return new Date(time.getTime() - (IST_TIME_ZONE_OFFSET * 60000));
}


module.exports = {getUtcTime, getLocalTime}
