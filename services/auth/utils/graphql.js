const Lokka = require('lokka').Lokka;
const Transport = require('lokka-transport-http').Transport;
const { config } = require('./config.js');

const endpoint =config.graphql_endpoint+"/v1/graphql";

const headers = {
    'x-hasura-admin-secret': process.env.GRAPHQL_ACCESS_KEY
};

const graphql = new Lokka({
    transport: new Transport(endpoint, {headers})
});

module.exports = { graphql, headers };
