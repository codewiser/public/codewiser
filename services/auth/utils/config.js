const path = require('path');
var endpoint = "http://graphql:8080";
console.log('global.UnitTest: '+global.UnitTest);

if(global.UnitTest!=undefined && global.UnitTest){
    process.env.NODE_ENV='development';
    endpoint = 'http://ubuntu/api';
}
console.log('process.env.NODE_ENV: '+process.env.NODE_ENV);

const config = {
    service: 'auth',
    issuer: 'urn:codewiser.in',
    audience: 'urn:codewiser.in',
    graphql_endpoint: endpoint,
    key_pair_path: path.resolve(__dirname) + '/../key-pair'
}

module.exports = { config };
