let jwt = require('jsonwebtoken');
const { config } = require('../utils/config.js');
const fs  = require('fs');
const publicKey = fs.readFileSync(config.key_pair_path+'/key.pem', 'utf8');

const verifyToken = (req, res, next) => {
    let token = req.headers['authorization']; // Express headers are auto converted to lowercase

    if (token) {
        // Remove 'Bearer ' from string
        token = token.slice(7, token.length);
        jwt.verify(token, publicKey, (err, decoded) => {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Token is not valid'
                });
            } else {
                req.decoded = decoded;
                req.token = token;
                next();
            }
        });
    } else {
        return res.json({
            success: false,
            message: 'Auth token is not supplied'
        });
    }
};

module.exports = {verifyToken};
