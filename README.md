# Documentation

This project contains multiple subprojects
>**install** - This subproject contains the scripts to start/stop individual components. *Only this is enough to run the platform, rest of the subprojects contain the source code*
>* **infra/schema** -  This subproject contains all db migrations and also scripts to create new migrations
>* **infra/applib** - This subproject contains the source code for npm package library used by all apps
>* **services/auth** -  This subproject contains the source code for authentication service
>* **apps** - This folder contains the 2 subprojects for 2 apps

Runtime requires docker <br/>
Install docker for your operating system from - https://docs.docker.com/engine/install/ <br/>

Rest of the process requires linux terminal. <br/>
On windows install WSL2 (Windows Subsystem for Linux) and then install Ubuntu app from Microsoft Store which provides Linux terminal in Windows

Open the Linux terminal (On Windows start newly installed Ubuntu application) <br/>
To run docker commands as any user
```bash
sudo groupadd docker
sudo usermod -aG docker $USER
```

Download and Install docker-compose
```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

Download the source code
```bash
git clone https://gitlab.com/codewiser/public/codewiser.git
```

Start the platform
```bash
cd ./codewiser/install
. ./setup.sh # Note the . before ./setup.sh This is important
./run.sh create # run.sh supports tab completion. Use tab completion to see all commands and their options
./run.sh start
```

To open the application goto *http://localhost* in any browser (Chrome preferred) 
> Make sure port 80 on locahost is free, else stop the application using port 80 <br/>
> Default *username*: **000000000** and *password*: **testing**

# Start the database console for schema changes

```bash
cd ./codewiser/infra/schema
./console.sh db/schema
```

To open the console application goto *http://localhost:9695* in any browser (Chrome preferred) <br/>
> Make sure port 9695 on locahost is free, else stop the application using port 9695 <br/>

Secret key is **GRAPHQL_ACCESS_KEY** variable value from ./codewiser/install/setup.sh file <br/>
Any changes made to the database schema or tables will generate new migration files into db/schema/migrations folder. <br/>
Commit these changes to source control so that other users in your team can get them. <br/>

# Apply Schema Changes from other users

To apply new schema changes from other users which are committed to source control

```bash
cd codewiser/infra/schema
git pull # or equivalent command to your source control
./build.sh
docker-compose up
```
For documentation on using console application refer to https://hasura.io/docs/latest/graphql/core/index.html

# Add new application

To add new application:
> Make a copy of ./codewiser/apps/app into ./codewiser/apps folder and rename it to \<your-app-name\> <br/>
> Change the **app** in below files to \<your-app-name\>
> * package.json
> * docker-compose.yml
> * nginx.conf ( location /app { & try_files $uri /app/index.html; lines ) <br/>

> Add an entry into **auth.services** table for \<your-app-name\> <br/>
> Grant access to app for users by adding entry into **auth.service_access** table <br/>
> Logout/Login to see new app in launcher <br/>

To build and run your new app
```bash
cd ./codewiser/apps/<your-app-name>
npm install
./build.sh # For production images ./build.sh prod
docker-compose up
```

# Credits
Thanks to Hasura (https://hasura.io/) for enabling this great platform <br/>
Thanks to Traefik (https://traefik.io/) for enabling seamless integration of all componenents


