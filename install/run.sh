
# Initialize our own variables:

function usage() {
    echo 'Usage: ./run.sh COMMAND [container1, container2, ...]'
    echo 'COMMAND'
    echo '  create => Create containers'
    echo '  clean => Remove Dangling Images'
    echo '  clean -a => Remove All Images'
    echo '  start => Start containers'
    echo '  stop => Stop containers'
    echo '  prune => Prune unused containers'
    echo '  rm => remove image'
    echo '  upgrade => upgrade containers to new image'
    echo '  reset => Recreated All Containers from Latest Images'
    echo 'CONTAINER'
    echo '  One or more container names, if none then all containers will be attempted.'
}

COMMAND="$1"
OPTION="$2"
shift
if [ "$COMMAND" = '-h' ]; then
    usage
    exit 1
elif [ "$COMMAND" = 'create' ]; then
    COMMAND='up --no-start'
elif [ "$COMMAND" = 'prune' ]; then
    docker container prune -f
    exit 1
elif [ "$COMMAND" = 'clean' ]; then
    if [ "$OPTION" = '-a' ]; then
        docker rmi -f $(docker images -q)
    else
        docker rmi -f $(docker images -f dangling=true -q)
    fi
    exit 1
elif [ "$COMMAND" = 'reset' ]; then
    docker stop $(docker ps -a -q)
    docker rm $(docker ps -a -q)
    docker image prune -a -f
    ./run.sh create
    ./run.sh start
elif [ "$COMMAND" = 'upgrade' ]; then
    if [ "$OPTION" = 'apps' ]; then
        declare -a apps=("app")
        for i in "${apps[@]}"
        do
            ./run.sh stop $i
            ./run.sh prune
            ./run.sh rm $i
            ./run.sh create $i
            ./run.sh start $i
        done
        exit 1
    elif [ "$OPTION" = 'services' ]; then
        declare -a services=("auth")
        for i in "${services[@]}"
        do
            ./run.sh stop $i
            ./run.sh prune
            ./run.sh rm $i
            ./run.sh create $i
            ./run.sh start $i
        done
        exit 1
    else
        ./run.sh stop $OPTION
        ./run.sh prune
        ./run.sh rm $OPTION
        ./run.sh create $OPTION
        ./run.sh start $OPTION
        exit 1
    fi
fi

CONTAINERS=$@

(./manage.sh "$COMMAND" "${CONTAINERS[@]}")
