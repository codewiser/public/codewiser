#!/bin/bash
echo "Copy logrotate file - sudo cp docker-container-logrotate /etc/logrotate.d/docker-container"
echo "Change file permission of acme.json in prod and test env - chmod 600 infra/traefik/acme.json"
echo "create .production file with production variables for prod deployment"
# Change these in production env
PRODUCTION_VARS_FILE=./.production
if [ -f "$PRODUCTION_VARS_FILE" ]; then
  source $PRODUCTION_VARS_FILE
  export $(cut -d= -f1 $PRODUCTION_VARS_FILE)
else
  export GRAPHQL_ACCESS_KEY="66A5B6A85884AB6AA1EC199D24538" # Generated from https://randomkeygen.com/ - 256-bit WEP Keys
  export EVENT_TRIGGER_HEADER="73EEC874881DE6135E2183C456C6B"
  export SERVICE_USER="1111111111"
  export SERVICE_PASSWORD="testing"
fi

if [ "$1" = "prod" ]; then
  export ENVIRONMENT=prod
elif [ "$1" = "test" ]; then
  export ENVIRONMENT=test
else
  export ENVIRONMENT=dev
fi

docker network ls | grep privatenet > /dev/null || docker network create --driver bridge  privatenet

source ./containers.sh

script="./run.sh"

commands=("create" "start" "stop" "prune" "rm" "clean" "reset", "upgrade")
containers=${!components[*]}

_script()
{
  _script_commands=${commands[@]}

    local cur prev opts
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    opts=${commands[@]}

    case "${commands[@]}" in  ("$prev "*|*" $prev "*|*"$prev") opts=${containers[@]} ;; esac

    COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    return 0
}
complete -F _script $script
