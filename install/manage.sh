
source ./containers.sh

COMMAND=$1

function get_commands {
    read -ra temp -d '' <<<"$1"
    for i in "${!ORDER[@]}";
    do
        x=${ORDER[$i]}
        commands[$i]="docker-compose -f ${components[$x]}/docker-compose.yml"
        if test -f "${components[$x]}/docker-compose."$ENVIRONMENT".yml"; then
            echo "including file - ${components[$x]}/docker-compose."$ENVIRONMENT".yml"
           commands[$i]="${commands[$i]} -f ${components[$x]}/docker-compose."$ENVIRONMENT".yml"
        fi
        commands[$i]="${commands[$i]} $COMMAND"
    done
}

reverse() {
    min=0; max=$(( ${#commands[@]}-1))
    while [[ $min -lt $max ]]
    do
        x="${commands[$min]}"
        commands[$min]="${commands[$max]}"
        commands[$max]=$x
        (( min++, max-- ))
    done
}

read -ra containers -d '' <<<"${!components[@]}"

if [ -z "$2" ]
then
    X=${containers[@]}
    Y=${component_start_order[@]}
else
    X=$2
    Y=$2
fi

read -ra CONTAINERS -d '' <<< "$X"
read -ra ORDER -d '' <<< "$Y"

# This needs to be optimized
for i in "${!ORDER[@]}";
do
    for ((j=0; j<${#containers[@]}; j++))
    do
        if [ "${CONTAINERS[$i]}" = "${containers[$j]}" ]
        then
            break
        fi
    done
    if [ $j -eq ${#containers[@]} ]
    then
        echo "Unknown Container ${CONTAINERS[$i]}"
        exit 1
    fi
done

declare -a commands=()
get_commands "${CONTAINERS[@]}"

case $COMMAND in
    "up --no-start" | "start")
        for c in "${commands[@]}";
        do
            $c
        done
        ;;
   "stop")
    # Reverse the actions
        reverse $commands
        for c in "${commands[@]}";
        do
            $c
        done
        ;;
    "rm")
        for c in "${CONTAINERS[@]}";
        do
            docker images --format="{{.Repository}} {{.ID}}" | grep $c | cut -d' ' -f2 | xargs docker rmi
        done
        ;;
   *)
        echo 'Invalid Command'
        ;;
esac
