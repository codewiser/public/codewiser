
declare -A components=(
        #[Container Name]=Container Path
        #Infrastructure containers
        [traefik]="./infra/traefik"
        [postgres]="./infra/postgres"
        [graphql]="./infra/graphql"
        [schema]="./infra/schema"
        #Service containers
        [auth]="./services/auth"
        #App containers
        [home]="./apps/home"
        [app]="./apps/app"
)

#This is due to bash limitation where associative array order is not retained
declare -a component_start_order=(
        #Infrastructure containers
        "traefik"
        "postgres"
        "graphql"
        "schema"
        #Service containers
        "auth"
        #App containers
        "home"
        "app"
)
