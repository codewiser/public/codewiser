#!/bin/bash

echo 'Running init_db.sh script'

# Create project database and its user
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER codewiser WITH PASSWORD 'codewiser123codewiser';
    CREATE DATABASE codewiser;
    ALTER DATABASE codewiser OWNER TO codewiser;
EOSQL

# Create project database specific configuration as superuser
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "codewiser" <<-EOSQL
    CREATE EXTENSION IF NOT EXISTS pgcrypto;
EOSQL

