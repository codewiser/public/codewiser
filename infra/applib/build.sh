TAG=$(cat package.json \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')

NAME=$(cat package.json \
  | grep name \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')

echo $NAME:$TAG

DOCKERFILE=Dockerfile

URLTOSEARCH="gitlab.com"
FILENAME="$HOME/.npmrc"
while read -r line
do
        # get the url
        EXTRACTEDURL=$(echo "$line" | grep -o '//.*/:' | sed 's/\/:/\//g')

        # get the token
        NPM_TOKEN=$(echo "$line" | grep -o '_authToken=.*' | sed 's/_authToken=//g')

        if [ "//$URLTOSEARCH/" == "$EXTRACTEDURL" ]; then
                echo "Token found"
        fi
done < "$FILENAME"

if [ -z "$1" ]
then
    echo 'Argument missing, buiding development image'
else
    if [ "$1" = 'prod' ]; then
        echo 'Building production image'
        DOCKERFILE=Dockerfile.prod
        if [ -z "$2" ]
        then
            echo 'Using NPM_TOKEN from home directory !!!'
        else
          NPM_TOKEN=$2
        fi
        TAG=$TAG$2
    fi
fi

docker build --build-arg APP_NAME=$NAME --build-arg NPM_TOKEN=$NPM_TOKEN -f $DOCKERFILE -t registry.gitlab.com/codewiser/public/codewiser/infra/$NAME:$TAG .
