
if [ -z "$1" ]
then
    npm publish
else
    if [ "$1" = '-u' ]; then
        npm unpublish -f
    fi
fi
