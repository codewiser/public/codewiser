import React from 'react';
// Import the storybook libraries
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
// Import our component from this folder
import { Header } from './Header';
import Signin from './Signin';

// Here we describe the stories we want to see of the Button. The component is
// pretty simple so we will just make two, one with text and one with emojis
// Simple call storiesOf and then chain .add() as many times as you wish
//
// .add() takes a name and then a function that should return what you want
// rendered in the rendering area

function login(){
	console.log('login called');
}

storiesOf('Header')
	.add('With Login', () => (
		<Header  authenticated={true}/>
	))
	.add('Without Login', () => (
		<Header authenticated={false}/>
	));
storiesOf('Signin')
	.add('Login Page', () => (
		<Signin login={login}/>
	));

