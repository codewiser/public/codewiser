export const USER_STATUS = {
    Active: 1,
    InActive: 2
};

export const ROLES = {
    admin: "admin",
    adminuser: "user"
}

export const ROLE_IDS = {
    admin: 1,
    user: 2
}
