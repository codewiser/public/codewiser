import { Paper, TextField } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import React, { Fragment } from "react";
import ErrorIcon from "@material-ui/icons/Error";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";

const useStyles = makeStyles(theme => ({
  root: {
    height: "100vh",
    backgroundImage: `url(/banner2.png)`
  },
  topWindow: {
    position: "absolute",
    height: "100%",
    width: "100%",
    background: "rgba(0,0,0,0.3)"
  },
  formMain: {
    background: "#ffffff",
    width: 363,
    padding: 0
  },
  paperChild: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  paper: {
    padding: 24,
    position: "absolute",
    width: 363,
    marginTop: 117
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  logo: {
    margin: "auto",
    top: 38,
    height: 60,
    width: 170
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    background: "#39A484",
    textTransform: "none",
    "&:hover": {
      background: "#39A484"
    }
  }
}));

export default function SignUp({
  error,
  success,
  showSignin,
  handleChange,
  handleSignUpSubmit
}) {
  const classes = useStyles();
  const [mobileNumber, SetMobileNumber] = React.useState("");

  function onInputChange(e) {
    const value = e.target.value;
    const onlyNums = value.replace(/[^0-9]/g, "");
    if (onlyNums.length < 10 || onlyNums.length === 10) {
      SetMobileNumber(onlyNums);
      handleChange(e);
    }
  }

  function Alert({ type, message }) {
    return (
      <div class="muiAlert" style={{ display: "flex" }} role="alert">
        <div class="MuiAlert-icon">
          {type == "success" ? (
            <CheckCircleIcon style={{ fontize: 14, color: "#4caf50" }} />
          ) : (
            <ErrorIcon style={{ fontize: 14, color: "#f44336" }} />
          )}
        </div>
        <div
          class="MuiAlert-message"
          style={type == "success" ? { color: "#4caf50" } : { color: "#f44336" }}
        >
          {message}
        </div>
      </div>
    );
  }

  function SignUpView() {
    return (
      <Fragment>
        <Typography component="h1" variant="h5">
          Sign Up
        </Typography>
        {error && <Alert type="error" message={error} />}
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="firstname"
            name="firstname"
            label="First Name"
            size="small"
            onChange={handleChange}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="lastname"
            name="lastname"
            label="Last Name"
            size="small"
            onChange={handleChange}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            name="email"
            label="Email"
            size="small"
            autoComplete="email"
            onChange={handleChange}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            value={mobileNumber}
            id="mobile"
            name="mobile"
            label="Mobile Number"
            size="small"
            autoComplete="mobile"
            pattern="^[0-9]*$"
            onChange={onInputChange}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            size="small"
            type="password"
            id="password"
            name="password"
            label="Password"
            autoComplete="current-password"
            onChange={handleChange}
          />
          <Button
            type="button"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleSignUpSubmit}
          >
            Sign Up
          </Button>
          <Grid container>
            <Grid item xs>
              <Link
                style={{ cursor: "pointer" }}
                onClick={showSignin}
                variant="body2"
              >
                Sign In
              </Link>
            </Grid>
          </Grid>
        </form>
      </Fragment>
    );
  }
  return (
    <div className={classes.root}>
      <Container component="main" className={classes.formMain}>
        <Paper square elevation={4} className={classes.paper}>
          <CssBaseline />
          <div className={classes.paperChild}>
            {SignUpView()}
          </div>
        </Paper>
      </Container>
    </div>
  );
}
