import {
	Avatar,
	Grid,
	GridList,
	GridListTile,
	Paper,
	Typography
} from '@material-ui/core';
import {
	Login,
	clearToken,
	getAppName,
	getUserName,
	refreshToken,
	verifyToken
} from './Auth';
import React, { Component, Fragment } from 'react';

import AccountCircle from '@material-ui/icons/AccountCircle';
import AppBar from '@material-ui/core/AppBar';
import AppsIcon from '@material-ui/icons/Apps';
import ChangePassword from './ChangePassword';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Signin from './Signin';
import Toolbar from '@material-ui/core/Toolbar';
import { withStyles } from '@material-ui/styles';

const parseDomain = require('parse-domain');

const homeAppName = 'home';
const ALERT_NAME = 'alerts';

var logRocketInitialised = false;

const registerLogRocket = () => {
	/*
    if(logRocketInitialised){ //Already initialised, start new session
        LogRocket.startNewSession();
        return;
    }
    if(process.env.NODE_ENV === "production"){  //Production mode
        const pasedLocation = parseDomain(window.location.host,{ customTlds: ["local"] }); //Not on developer PC
        if(pasedLocation && (pasedLocation.tld != "local")){
            if(verifyToken() != null){   //authenticated user
                LogRocket.init('erbbgp/codewiser');
                LogRocket.identify(getUserId(), {
                    name: getUserName(),
                    mobile: getUserMobile(),
                    email: getUserEmail(),
                    appname: getAppName(),
                    role: getRole(),
                });
                logRocketInitialised = true;
            }
        }
    }
    */
};

export var redirectToLogin = () => {
	this.logout();
};

const styles = (theme) => ({
	Muiroot: {
		backgroundColor: 'transparent'
	},
	MuiDrawer: {
		width: '40%',
		height: '10%',
		margin: 'auto',
		background: '#39A484',
		overflow: 'hidden auto'
	},
	root: {
		padding: 24,
		width: '100%',
		margin: 'auto'
	},
	paper: {
		boxShadow: '-2px 2px 9px 0px rgba(50, 50, 50, 0.19)',
		margin: '0 1rem 2rem',
		padding: 0,
		textAlign: 'center',
		cursor: 'pointer',
		display: 'grid',
		alignItems: 'center',
		justifyContent: 'center',
		transition: 'all .2s ease-in-out',
		'&:hover': {
			transform: 'scale(1.3)',
			zIndex: 9,
			borderRadius: 13
		}
	},
	content: {
		paddingTop: 150,
		textAlign: 'center'
	},
	appSidebar: { padding: 0 },
	gridList: {
		margin: 0,
		paddingTop: 10
	},
	image: {
		marginTop: 10,
		display: 'inline-block',
		maxWidth: '100%',
		width: 100
	}
});

class HeaderPure extends Component {
	constructor(props) {
		super(props);
		this.payload = null;
		this.state = {
			authenticated: false,
			drawerVisible: false,
			menuAnchorEl: null,
			showChangePassword: false,
			menuVisible: false
		};

		this.renderServices = this.renderServices.bind(this);
		this.renderHeaderLinks = this.renderHeaderLinks.bind(this);
		this.login = this.login.bind(this);
		this.logout = this.logout.bind(this);

		this.toggleDrawer = this.toggleDrawer.bind(this);
		this.toggleMenu = this.toggleMenu.bind(this);
		this.togglePasswordChange = this.togglePasswordChange.bind(this);
		this.handleCloseMenu = this.handleCloseMenu.bind(this);
		redirectToLogin = redirectToLogin.bind(this);
	}

	componentDidMount() {
		const loader = document.querySelector('.loader');
		loader ? loader.classList.add('loader--hide') : '';
		this.payload = verifyToken();
		if (this.payload != null) {
			this.setState({ authenticated: true });
			registerLogRocket();
		} else {
			refreshToken().then((result) => {
				if (result == 'OK') {
					this.payload = verifyToken();
					this.setState({ authenticated: this.payload != null });
					registerLogRocket();
				}
			});
		}
	}

	login(username, password) {
		var promise = new Promise((resolve, reject) => {
			Login(username, password).then((result) => {
				if (result == 'OK') {
					this.payload = verifyToken();
					this.setState({ authenticated: this.payload != null });
					registerLogRocket();
				}
				resolve(result);
			});
		});
		return promise;
	}

	logout() {
		clearToken();
		this.setState({ authenticated: false, menuVisible: false });
		window.location.href = window.location.origin;
	}

	toggleDrawer() {
		this.setState({ drawerVisible: !this.state.drawerVisible });
	}
	toggleMenu(event) {
		this.setState({
			menuVisible: !this.state.menuVisible,
			menuAnchorEl: event.currentTarget
		});
	}
	togglePasswordChange() {
		const showChangePassword = !this.state.showChangePassword;
		this.setState({
			...this.state,
			menuVisible: false,
			showChangePassword: showChangePassword
		});
	}
	handleCloseMenu() {
		{
			this.setState({ menuVisible: false });
		}
	}

	getAppNameDisplayString() {
		appName = getAppName();
		if (this.payload) {
			var appName = this.payload.roles.filter((role) => {
				if (role.service == getAppName()) {
					return role.display_name;
				}
			});
			if (appName.length == 0) {
				appName = getAppName();
			} else {
				appName = appName[0].display_name;
			}
		}
		return appName;
	}

	renderServices() {
		const { classes } = this.props;
		if (this.payload) {
			/*const ROLES_ARRAY = this.payload.roles.sort((a, b) =>
				a.service < b.service ? -1 : a.service > b.service ? 1 : 0
			);*/
			const ROLES_ARRAY = this.payload.roles;
			return (
				<div className={classes.appSidebar}>
					<Grid container justify="center">
						<Grid item lg={11} xs={11}>
							<GridList
								cellHeight={160}
								className={classes.gridList}
								style={{ margin: 0 }}
								cols={3}
							>
								{ROLES_ARRAY.map((role) => {
									const SvgSrcIcon = `/images/app_icons/${role.service}.svg`;
									return (
										<Paper
											key={role.service}
											className={classes.paper}
											style={{ width: 50, cursor: 'pointer', height: 30 }}
											onClick={() =>
												(window.location =
													role.service == homeAppName
														? '/'
														: '/' + role.service)
											}
										>
											<GridListTile key={role.service}>
												<Avatar
													variant="square"
													style={{ margin: 'auto', width: '20px', height: '20px' }}
													alt={role.display_name}
													src={SvgSrcIcon}
												/>
											</GridListTile>
											<span style={{ fontSize: 10 }}>{role.display_name}</span>
										</Paper>
									);
								})}
							</GridList>
						</Grid>
					</Grid>
				</div>
			);
		} else {
			return '';
		}
	}

	renderHeaderLinks() {
		const { classes } = this.props;
		const { showChangePassword } = this.state;
		return (
			<Fragment>
				{showChangePassword && (
					<ChangePassword
						showModel={true}
						onClose={this.togglePasswordChange}
					/>
				)}
				<AppBar
					position="fixed"
					style={{
						background: '#39A484',
						boxShadow: '-2px 2px 9px 0px rgba(50, 50, 50, 0.19)'
					}}
				>
					<Toolbar>
						{/* <Toolbar variant="dense"> */}
						<IconButton
							edge="start"
							color="inherit"
							aria-label="menu"
							onClick={this.toggleDrawer}
						>
							<AppsIcon />
						</IconButton>
						<Typography variant="h6" style={{ flex: 1 }}>
							Applications
						</Typography>
						<Typography variant="h6" style={{ flex: 1 }}>
							{this.getAppNameDisplayString()}
						</Typography>

						<Drawer
							anchor="left"
							classes={{ paper: classes.MuiDrawer, root: classes.Muiroot }}
							open={this.state.drawerVisible}
							onClose={this.toggleDrawer}
						>
							{this.renderServices()}
						</Drawer>
						<IconButton
							aria-label="account of current user"
							aria-controls="menu-appbar"
							aria-haspopup="true"
							onClick={this.toggleMenu}
							color="inherit"
						>
							<AccountCircle />
							{getUserName()}
						</IconButton>
						<Menu
							id="menu-appbar"
							anchorEl={this.state.menuAnchorEl}
							anchorOrigin={{
								vertical: 'top',
								horizontal: 'right'
							}}
							keepMounted
							transformOrigin={{
								vertical: 'top',
								horizontal: 'right'
							}}
							onClose={this.handleCloseMenu}
							open={this.state.menuVisible}
						>
							<MenuItem onClick={this.togglePasswordChange}>
								Change Password
							</MenuItem>
							<MenuItem onClick={this.logout}>Logout</MenuItem>
						</Menu>
					</Toolbar>
				</AppBar>
				{this.props.children}
			</Fragment>
		);
	}

	render() {
		return (
			<Fragment>
				{this.state.authenticated ? (
					this.renderHeaderLinks()
				) : (
					<Signin login={this.login} />
				)}
			</Fragment>
		);
	}
}

const Header = withStyles(styles)(HeaderPure);
export { Header };
