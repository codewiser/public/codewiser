import { Paper, TextField } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import React, { Fragment } from "react";
import ErrorIcon from "@material-ui/icons/Error";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";

const useStyles = makeStyles(theme => ({
  root: {
    height: "100vh",
    backgroundImage: `url(/banner2.png)`
  },
  topWindow: {
    position: "absolute",
    height: "100%",
    width: "100%",
    background: "rgba(0,0,0,0.3)"
  },
  formMain: {
    background: "#ffffff",
    width: 363,
    padding: 0
  },
  paperChild: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  paper: {
    padding: 24,
    position: "absolute",
    width: 363,
    marginTop: 117
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  logo: {
    margin: "auto",
    top: 38,
    height: 60,
    width: 170
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    background: "#39A484",
    textTransform: "none",
    "&:hover": {
      background: "#39A484"
    }
  }
}));

function Alert({ type, message }) {
  return (
    <div class="muiAlert" style={{ display: "flex" }} role="alert">
      <div class="MuiAlert-icon">
        {type == "success" ? (
          <CheckCircleIcon style={{ fontize: 14, color: "#4caf50" }} />
        ) : (
          <ErrorIcon style={{ fontize: 14, color: "#f44336" }} />
        )}
      </div>
      <div
        class="MuiAlert-message"
        style={type == "success" ? { color: "#4caf50" } : { color: "#f44336" }}
      >
        {message}
      </div>
    </div>
  );
}

export default function Login({
  isSigninPage,
  error,
  success,
  handleChange,
  handleSigninSubmit,
  showForgotPassword,
  handleForgotPasswordSubmit,
  showSignin,
  showSignUp,
  isOtpRequestScreen,
  resendOtp,
  otp,
  password,
  username,
  newPassword
}) {
  const classes = useStyles();
  const [mobileNumber, SetMobileNumber] = React.useState("");

  function onInputChange(e) {
    const value = e.target.value;
    const onlyNums = value.replace(/[^0-9]/g, "");
    if (onlyNums.length < 10 || onlyNums.length === 10) {
      SetMobileNumber(onlyNums);
      handleChange(e);
    }
  }
  function SignInView() {
    const disabled = mobileNumber && password ? false : true;
    return (
      <Fragment>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            value={mobileNumber}
            id="username"
            name="username"
            label="Mobile Number"
            size="small"
            autoComplete="mobile"
            autoFocus
            pattern="^[0-9]*$"
            onChange={onInputChange}
            onKeyDown={event => {
              if (event.keyCode === 13 && !disabled) {
                handleSigninSubmit();
              }
            }}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            value={password}
            size="small"
            type="password"
            id="password"
            name="password"
            label="Password"
            autoComplete="current-password"
            onChange={handleChange}
            onKeyDown={event => {
              if (event.keyCode === 13 && !disabled) {
                handleSigninSubmit();
              }
            }}
          />
          <Button
            disabled={disabled}
            type="button"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleSigninSubmit}
          >
            Sign in
          </Button>
          <Grid container>
            <Grid item xs>
              <Link
                style={{ cursor: "pointer" }}
                onClick={showForgotPassword}
                variant="body2"
              >
                Forgot password?
              </Link>
            </Grid>
            <Grid item xs>
              <Link
                style={{ cursor: "pointer" }}
                onClick={showSignUp}
                variant="body2"
              >
                Sign Up
              </Link>
            </Grid>
          </Grid>
        </form>
      </Fragment>
    );
  }
  function OTPRequestScreen() {
    return (
      <Fragment>
        {" "}
        <Typography component="h1" variant="h5">
          Forgot password
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            value={mobileNumber}
            id="username"
            name="username"
            label="Mobile Number"
            size="small"
            autoComplete="mobile"
            autoFocus
            pattern="^[0-9]*$"
            onChange={onInputChange}
          />
          <Button
            disabled={!mobileNumber}
            type="button"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleForgotPasswordSubmit}
          >
            Send OTP
          </Button>
          <Grid container>
            <Grid item xs>
              <Link
                style={{ cursor: "pointer" }}
                onClick={showSignin}
                variant="body2"
              >
                Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </Fragment>
    );
  }
  function SetNewPasswordScreen() {
    const disabledSet = otp && newPassword ? false : true;
    return (
      <Fragment>
        {" "}
        <Typography component="h1" variant="h5">
          Forgot password
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            value={otp}
            id="otp"
            name="otp"
            label="OTP"
            size="small"
            autoFocus
            onChange={handleChange}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            size="small"
            value={newPassword}
            type="password"
            id="newPassword"
            name="newPassword"
            label="New password"
            autoComplete="current-password"
            onChange={handleChange}
          />
          <Button
            disabled={disabledSet}
            type="button"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleForgotPasswordSubmit}
          >
            Set new password
          </Button>
          <Grid container>
            <Grid item xs>
              <Link
                style={{ cursor: "pointer" }}
                onClick={showSignin}
                variant="body2"
              >
                Sign in
              </Link>
            </Grid>
            <Grid item>
              <Link
                style={{ cursor: "pointer" }}
                onClick={resendOtp}
                variant="body2"
              >
                {"Resend OTP"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </Fragment>
    );
  }
  return (
    <div className={classes.root}>
      <Container component="main" className={classes.formMain}>
        <Paper square elevation={4} className={classes.paper}>
          <CssBaseline />
          <div className={classes.paperChild}>
            {error && <Alert type="error" message={error} />}
            {isSigninPage
              ? SignInView()
              : isOtpRequestScreen
              ? OTPRequestScreen()
              : SetNewPasswordScreen()}
          </div>
        </Paper>
      </Container>
    </div>
  );
}
