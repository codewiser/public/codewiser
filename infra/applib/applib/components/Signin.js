import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { requestOtp, changePasswordWithOtp, requestResendOtp, requestSignUp } from "./Auth";
import Login from "./Login";
import SignUp from "./SignUp";

const SCREEN_SIGNIN = 1;
const SCREEN_FORGOT_PASSWORD = 2;
const SCREEN_SIGNUP = 3;

const styles = theme => ({
  paper: {
    position: "absolute",
    width: theme.spacing(30),
    padding: theme.spacing(4)
  }
});

class Signin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screen: SCREEN_SIGNIN,
      username: "",
      password: "",
      newPassword: "",
      firstname: "",
      lastname: "",
      email: "",
      mobile: "",
      success: "",
      error: "",
      otp: "",
      otpRequested: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSigninSubmit = this.handleSigninSubmit.bind(this);
    this.handleForgotPasswordSubmit = this.handleForgotPasswordSubmit.bind(this);
    this.handleSignUpSubmit = this.handleSignUpSubmit.bind(this);
    this.showSignin = this.showSignin.bind(this);
    this.showForgotPassword = this.showForgotPassword.bind(this);
    this.showSignUp = this.showSignUp.bind(this);
    this.resendOtp = this.resendOtp.bind(this);
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSigninSubmit() {
    const { username, password } = this.state;

    this.props.login(username, password).then(result => {
      if (result != "OK") {
        this.setState({
          error: result,
          success: ""
        });
      }
    });
  }

  handleSignUpSubmit() {
    requestSignUp({
      firstname: this.state.firstname,
      lastname: this.state.lastname,
      email: this.state.email,
      mobile: this.state.mobile,
      password: this.state.password,
    }).then(result => {
      if (result == "OK") {
        this.setState({ screen: SCREEN_SIGNIN, error: "", success: "" });
      } else {
        this.setState({
          error: result,
          success: ""
        });
      }
    });
  }

  handleForgotPasswordSubmit() {
    const { username, otp, newPassword } = this.state;

    if (this.state.otpRequested) {
      changePasswordWithOtp(username, otp, newPassword).then(result => {
        if (result == "OK") {
          this.setState({ error: "", success: "" });
          this.showSignin();
        } else {
          this.setState({
            error: result,
            success: ""
          });
        }
      });
    } else {
      requestOtp(username).then(result => {
        if (result == "OK") {
          this.setState({ otpRequested: true, error: "", success: "" });
        } else {
          this.setState({
            error: result,
            success: ""
          });
        }
      });
    }
  }

  resendOtp() {
    const { username } = this.state;

    requestResendOtp(username).then(result => {
      if (result == "OK") {
        this.setState({ success: "OTP sent successfully", error: "" });
      } else {
        this.setState({
          error: result,
          success: ""
        });
      }
    });
  }

  render() {
    switch (this.state.screen) {
      case SCREEN_SIGNIN:
        return this.renderSignin();
      case SCREEN_FORGOT_PASSWORD:
        return this.renderForgotPassword();
      case SCREEN_SIGNUP:
        return this.renderSignUp();
    }
    return renderSignin();
  }

  showSignin() {
    this.setState({ error: "", screen: SCREEN_SIGNIN, otpRequested: false });
  }

  showSignUp() {
    this.setState({ error: "", screen: SCREEN_SIGNUP, otpRequested: false });
  }

  showForgotPassword() {
    this.setState({
      error: "",
      screen: SCREEN_FORGOT_PASSWORD,
      otpRequested: false
    });
  }

  renderSignin() {
    const { username, password } = this.state;
    const { classes } = this.props;
    return (
      <Login
        isSigninPage={true}
        handleChange={this.handleChange}
        success={this.state.success}
        error={this.state.error}
        username={username}
        password={password}
        handleSigninSubmit={this.handleSigninSubmit}
        showForgotPassword={this.showForgotPassword}
        showSignUp={this.showSignUp}
      />
    );
  }

  renderSignUp() {
    const { classes } = this.props;
    return (
      <SignUp
        success={this.state.success}
        error={this.state.error}
        showSignin={this.showSignin}
        handleChange={this.handleChange}
        handleSignUpSubmit={this.handleSignUpSubmit}
      />
    );
  }

  renderForgotPassword() {
    const { username, otp, password, newPassword } = this.state;
    const { classes } = this.props;
    return (
      <Login
        isSigninPage={false}
        handleChange={this.handleChange}
        success={this.state.success}
        otp={otp}
        newPassword={newPassword}
        error={this.state.error}
        username={username}
        password={password}
        handleSigninSubmit={this.handleSigninSubmit}
        showForgotPassword={this.showForgotPassword}
        handleForgotPasswordSubmit={this.handleForgotPasswordSubmit}
        showSignin={this.showSignin}
        showSignUp={this.showSignUp}
        isOtpRequestScreen={!this.state.otpRequested}
        resendOtp={this.resendOtp}
      />
    );
  }
}

export default withStyles(styles)(Signin);
