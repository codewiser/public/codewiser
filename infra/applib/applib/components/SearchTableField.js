import React, { Component } from 'react'
import { withApollo, Query } from 'react-apollo';
import gql from 'graphql-tag';
import MuiDownshift from 'mui-downshift';
import { ListItem, ListItemText } from '@material-ui/core';

class SearchTableFieldPure extends Component {
    constructor(props){
        super(props);
        this.state = {
            userSearch: '',
            data: props.selectedItem,
        }
        this.handleStateChange = this.handleStateChange.bind(this);
        this.handleSelected = this.handleSelected.bind(this);
    }

    handleStateChange(changes) {
        if (typeof changes.inputValue === 'string') {
            if(changes.selectedItem == undefined || changes.selectedItem == null){
                this.setState({userSearch:  changes.inputValue });
            }
        }
    }

    handleSelected(selectedItem) {
        this.setState({userSearch:  '', data: selectedItem});
        this.props.onSelect(selectedItem);
    }

    render () {
        const limit=10;
        const table = this.props.schema+'_'+this.props.table;
        const field = this.props.field;
        const whereType = this.props.schema+'_'+this.props.table+'_bool_exp';
        const extraFields = (this.props.extraFields != undefined) ? this.props.extraFields : '';
        const QUERY = gql`
            query getData($search: String!, $where: ${whereType}) {
                ${table}(where: {_and: [$where, {${field}: {_ilike: $search}}]},limit: ${limit}){
                    id,
                    ${field},
                    ${extraFields}
                }
            }
        `
        return (
            <Query query={QUERY} fetchPolicy="no-cache" variables={{search: '%'+this.state.userSearch+'%', where: this.props.where}}>
                {({ loading, error, data }) => {
                if (error) return <div>Error - {error.message}</div>
                const list = [];
                if(!loading){
                    data[table].forEach(el=>{
                        list.push({id: el.id, label: el[field], extraFields: el});
                    })
                }
                return (
                    <MuiDownshift
                        items={list}
                        menuItemCount={"2"}
                        getListItem={({ getItemProps, item }) =>
                                        item ? (
                                        <ListItem button {...getItemProps()}>
                                            <ListItemText primary={item.label} />
                                        </ListItem>
                                        ) : loading ? (
                                        <ListItem button disabled>
                                            <ListItemText primary={<span style={{ fontStyle: 'italic' }}>Loading...</span>} />
                                        </ListItem>
                                        ) : (
                                        <ListItem button disabled>
                                            <ListItemText primary={<span style={{ fontStyle: 'italic' }}>No items found</span>} />
                                        </ListItem>
                                        )
                                    }
                        showEmpty
                        getInputProps={ () => {
                            return {
                                        label: this.props.title,
                                        disabled: this.props.disabled
                                    }
                        }}
                        selectedItem={this.state.data}
                        includeFooter={loading}
                        loading={loading}
                        onSelect={this.handleSelected}
                        onStateChange={this.handleStateChange}
                    />
                    )
                }}
            </Query>
        )
    }
}

const SearchTableField = withApollo(SearchTableFieldPure);
export {SearchTableField}
