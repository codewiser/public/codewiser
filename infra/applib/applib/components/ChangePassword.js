import React, { Component, Fragment } from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Modal from "@material-ui/core/Modal";
import { withStyles } from "@material-ui/core/styles";
import { changePassword } from "./Auth";
import { Avatar, IconButton } from "@material-ui/core";
import Container from "@material-ui/core/Container";
import CloseIcon from "@material-ui/icons/Close";
import CssBaseline from "@material-ui/core/CssBaseline";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import ErrorIcon from "@material-ui/icons/Error";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";

function Alert({ type, message }) {
  return (
    <div
      class="muiAlert"
      style={{ display: "flex", float: "left" }}
      role="alert"
    >
      <div class="MuiAlert-icon">
        {type == "success" ? (
          <CheckCircleIcon style={{ fontize: 14, color: "#4caf50" }} />
        ) : (
          <ErrorIcon style={{ fontize: 14, color: "#f44336" }} />
        )}
      </div>
      <div
        class="MuiAlert-message"
        style={type == "success" ? { color: "#4caf50" } : { color: "#f44336" }}
      >
        {message}
      </div>
    </div>
  );
}

const styles = theme => ({
  paper: {
    position: "absolute",
    width: theme.spacing(30),
    padding: theme.spacing(4)
  },
  topWindow: {
    position: "absolute",
    height: "100%",
    width: "100%",
    background: "rgba(0,0,0,0.3)"
  },
  formMain: {
    background: "#ffffff",
    width: 363,
    padding: 0
  },
  paperChild: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  paper: {
    padding: 24,
    position: "absolute",
    width: 363,
    marginTop: 117
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    background: "#304179",
    textTransform: "none",
    "&:hover": {
      background: "#3f51b5"
    }
  }
});

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      oldPassword: "",
      newPassword: "",
      error: "",
      success: "",
      modalVisible: props.showModel,
      passwordChanged: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.closeModel = this.closeModel.bind(this);
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSubmit() {
    const { username, oldPassword, newPassword } = this.state;

    changePassword(username, oldPassword, newPassword).then(result => {
      if (result == "OK") {
        this.setState({
          success: "Password changed successfully",
          error: "",
          passwordChanged: true
        });
      } else {
        this.setState({
          error: result,
          success: ""
        });
      }
    });
  }

  closeModel() {
    this.setState({ modalVisible: false });
    this.props.onClose();
  }

  render() {
    const { username, oldPassword, newPassword } = this.state;
    const disabled = username && oldPassword && newPassword ? false : true;
    const { classes } = this.props;
    return (
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        style={{ alignItems: "center", justifyContent: "center" }}
        open={this.props.showModel}
        onClose={this.props.onClose}
      >
        <div className={classes.root}>
          <Container component="main" className={classes.formMain}>
            <Paper square elevation={4} className={classes.paper}>
              <CssBaseline />
              <div className={classes.paperChild}>
                {!this.state.passwordChanged ? (
                  <Fragment>
                    <Avatar className={classes.avatar}>
                      <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                      Change password
                    </Typography>
                    <Grid container justify="center" direction="column">
                      <TextField
                        id="username"
                        name="username"
                        label="Re-Enter Username"
                        placeholder="Mobile/Email address"
                        margin="normal"
                        variant="outlined"
                        value={username}
                        autoFocus
                        onChange={this.handleChange}
                        InputLabelProps={{
                          shrink: true
                        }}
                      />
                      <TextField
                        id="oldpassword"
                        name="oldPassword"
                        label="Old Password"
                        placeholder="Old Password"
                        margin="normal"
                        variant="outlined"
                        type="password"
                        value={oldPassword}
                        onChange={this.handleChange}
                        InputLabelProps={{
                          shrink: true
                        }}
                      />
                      <TextField
                        id="newpassword"
                        name="newPassword"
                        label="New Password"
                        placeholder="New Password"
                        margin="normal"
                        variant="outlined"
                        type="password"
                        value={newPassword}
                        onChange={this.handleChange}
                        InputLabelProps={{
                          shrink: true
                        }}
                      />
                      {this.state.error && (
                        <Alert type="error" message={this.state.error} />
                      )}
                      <Button
                        disabled={disabled}
                        type="button"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={this.handleSubmit}
                      >
                        Change Password
                      </Button>
                    </Grid>
                  </Fragment>
                ) : (
                  <div style={{ width: "100%" }}>
                    {this.state.success && (
                      <Alert type="success" message={this.state.success} />
                    )}

                    <IconButton
                      onClick={this.closeModel}
                      style={{
                        float: "right",
                        cursor: "pointer",
                        marginTop: -16
                      }}
                    >
                      <CloseIcon />
                    </IconButton>
                  </div>
                )}
              </div>
            </Paper>
          </Container>
        </div>
      </Modal>
    );
  }
}

export default withStyles(styles)(ChangePassword);
