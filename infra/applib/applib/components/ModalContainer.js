import React from 'react';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

export function ModalContainer(props) {
	const {
		width,
		onClose,
		title,
		headerDisable,
		footerDisable,
		ischatbox,
		UIContiner,
		isPrimaryButton,
		isCloseIconEnable,
		open,
		...other
	} = props;
	const radioGroupRef = React.useRef(null);

	const handleEntering = () => {
		if (radioGroupRef.current != null) {
			radioGroupRef.current.focus();
		}
	};
	const MenuTheme = createMuiTheme({
		overrides: {
			MuiDialog: {
				paperScrollPaper: {
					display: ischatbox ? 'inline-table' : 'flex',
					borderRadius: ischatbox ? 4 : 0,
					width: width ? width : null
				}
			},
			MuiDialogActions: {
				root: {
					justifyContent: isPrimaryButton ? 'flex-start' : 'flex-end'
				}
			}
		}
	});
	const handleCancel = () => {
		onClose();
	};

	return (
		<ThemeProvider theme={MenuTheme}>
			<Dialog
				disableBackdropClick
				disableEscapeKeyDown
				maxWidth="lg"
				onEntering={handleEntering}
				aria-labelledby="confirmation-dialog-title"
				open={open}
				{...other}
			>
				{!headerDisable && (
					<DialogTitle id="confirmation-dialog-title">
						<span style={{ fontFamily: 'CentraleSansMedium' }}>{title}</span>
						{isCloseIconEnable && (
							<IconButton
								onClick={handleCancel}
								style={{
									cursor: 'pointer',
									position: 'absolute',
									right: '10px',
									top: '4px'
								}}
							>
								<CloseIcon />
							</IconButton>
						)}
					</DialogTitle>
				)}
				<DialogContent
					dividers
					style={{
						padding: ischatbox ? 0 : '8px 24px',
						marginTop: ischatbox ? -1 : 0,
						overflowX: 'hidden'
					}}
				>
					{UIContiner}
				</DialogContent>
				{!footerDisable && (
					<DialogActions>
						<Button
							autoFocus
							onClick={handleCancel}
							color="primary"
							style={{
								textTransform: 'none',
								borderRadius: '0'
							}}
						>
							Cancel
						</Button>
					</DialogActions>
				)}
			</Dialog>
		</ThemeProvider>
	);
}
