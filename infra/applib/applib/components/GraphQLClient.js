import { ApolloClient } from 'apollo-client'
import { Observable } from 'apollo-link';
import { createHttpLink } from 'apollo-link-http'
import { onError } from "apollo-link-error";
import { InMemoryCache } from 'apollo-cache-inmemory'
import { setContext } from 'apollo-link-context';
import { getToken, refreshToken, setAppName, getRole } from './Auth'
import { redirectToLogin } from './Header';

const addApiHeaders = (headers) => {
  headers = {
    ...headers,
    authorization:`Bearer ${getToken()}`,
    'x-hasura-role': getRole()
  }
  return headers;
}

const customFetch = (uri, options) => {
  return fetch(uri, options);
}

const promiseToObservable = (promise) => {
    return new Observable((subscriber) => {
        promise.then((value) => {
            if(value == "OK"){
                if (subscriber.closed) return;
                subscriber.next(value);
                subscriber.complete();
            }
            else {
                subscriber.error(err);
                redirectToLogin();
            }
        });
    });
}

const retryLink = onError(({ graphQLErrors, networkError, operation, forward }) => {
    if(graphQLErrors){
        for (let err of graphQLErrors) {
            if(err.message == "Could not verify JWT: JWTExpired"){
                return promiseToObservable(refreshToken()).flatMap(() => {
                    operation.setContext({headers: addApiHeaders(operation.getContext().headers)});
                    return forward(operation);
                });
            }
            if (err.message === "Your current role is not in allowed roles"){
                alert( 'You do not have permission!');
            }
        }
    }
    if (networkError) {
        console.log(`[Network error]: ${networkError}`);
    }
});

const httpLink = createHttpLink({
    uri: '/api/v1/graphql',
})

const authLink = setContext((_, { headers }) => {
    return {
        headers: {
            ...addApiHeaders(headers)
        }
    }
});

const graphqlClient = (appname) => {
    setAppName(appname);
    return new ApolloClient({
        link: authLink.concat(retryLink).concat(httpLink),
        cache: new InMemoryCache(),
    })
}

export {graphqlClient}
