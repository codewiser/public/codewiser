import axios from 'axios';
import jwt from 'jsonwebtoken';

const verifyOptions = {
  issuer: 'urn:codewiser.in',
  audience: 'urn:codewiser.in',
};

var AppName = '';
var tokenPayload = null;

const Login = (username, password) => {
  var promise = new Promise((resolve, reject) => {
    axios
      .post(`/auth/auth/get-token`, {
        username,
        password
      })
      .then((response) => {
        if (response.status == 200 && response.data && response.data.token) {
          setToken(response.data);
        } else {
          clearToken();
        }
        resolve(response.data.status);
      });
  });
  return promise;
};

const refreshToken = async () => {
  var promise = new Promise((resolve, reject) => {
    const token = getToken();

    if (token) {
      if (token == undefined || token == 'undefined' || token == null) {
        clearToken();
        resolve('Error 1');
        return;
      }
      axios
        .post(`/auth/auth/refresh-token`, { token })
        .then((response) => {
          if (response.status == 200) {
            setToken({
              token: response.data.token,
              public_key: getPublicKey(),
            });
          } else {
            clearToken();
          }
          resolve(response.data.status);
        });
    } else {
      clearToken();
      resolve('Error 2');
    }
  });

  return promise;
};

const changePassword = async (username, oldPassword, newPassword) => {
  var promise = new Promise((resolve, reject) => {
    axios
      .post(`/auth/auth/reset-password`, {
        username: username,
        old_password: oldPassword,
        new_password: newPassword,
      })
      .then((response) => {
        if (response.status == 200) {
          resolve(response.data.status);
        } else {
          resolve(response.statusText);
        }
      });
  });
  return promise;
};

const requestOtp = async (username) => {
  var promise = new Promise((resolve, reject) => {
    axios
      .post(`/auth/auth/request-reset-password`, {
        username: username
      })
      .then((response) => {
        if (response.status == 200) {
          resolve(response.data.status);
        } else {
          resolve(response.statusText);
        }
      });
  });
  return promise;
};

const requestResendOtp = async (username) => {
  var promise = new Promise((resolve, reject) => {
    axios
      .post(`/auth/auth/resend-password-otp`, {
        username: username
      })
      .then((response) => {
        if (response.status == 200) {
          resolve(response.data.status);
        } else {
          resolve(response.statusText);
        }
      });
  });
  return promise;
};

const changePasswordWithOtp = async (username, otp, newPassword) => {
  var promise = new Promise((resolve, reject) => {
    axios
      .post(`/auth/auth/reset-password`, {
        username: username,
        otp: otp,
        new_password: newPassword,
      })
      .then((response) => {
        if (response.status == 200) {
          resolve(response.data.status);
        } else {
          resolve(response.statusText);
        }
      });
  });
  return promise;
};

const requestSignUp = async(data) => {
  var promise = new Promise((resolve, reject) => {
    axios
      .post(`/auth/auth/signup`, data)
      .then((response) => {
        if (response.status == 200) {
          console.log(response.data);
          resolve(response.data.status);
        } else {
          resolve(response.statusText);
        }
      });
  });
  return promise;
}

const verifyToken = () => {
  const token = getToken();
  if (token != null) {
    try {
      return jwt.verify(token, getPublicKey(), verifyOptions);
    } catch (e) {
      console.log(e.message);
    }
  }
  return null;
};

const clearToken = () => {
  tokenPayload = null;
  localStorage.setItem('token', null);
  localStorage.setItem('public_key', null);
};

const setToken = (data) => {
  tokenPayload = null;
  localStorage.setItem('token', data.token);
  localStorage.setItem('public_key', data.public_key);
};

const setAppName = (appname) => {
  AppName = appname;
};

const getTokenPayload = () => {
  const token = getToken();
  if (tokenPayload == null && token != null) {
    tokenPayload = jwt.decode(token, verifyOptions);
  }

  return tokenPayload;
};

const getRole = () => {
  let role = 'guest';

  if (getTokenPayload() != null) {
    const appname = getAppName();
    getTokenPayload().roles.forEach((element) => {
      if (element.service == appname) {
        role = element.role;
      }
    });
    return role;
  }
};

const getUserName = () =>
  getTokenPayload()
    ? getTokenPayload().first_name + ' ' + getTokenPayload().last_name
    : '';
const getUserMobile = () => (getTokenPayload() ? getTokenPayload().mobile : '');
const getUserEmail = () => (getTokenPayload() ? getTokenPayload().email : '');
const getUserId = () => (getTokenPayload() ? getTokenPayload().user_id : 0);
const getAppName = () => AppName;
const getToken = () => localStorage.getItem('token');
const getPublicKey = () => localStorage.getItem('public_key');

export {
  Login,
  refreshToken,
  clearToken,
  verifyToken,
  setAppName,
  changePassword,
  requestOtp,
  changePasswordWithOtp,
  requestResendOtp,
  requestSignUp,
  getRole,
  getUserName,
  getUserId,
  getUserMobile,
  getUserEmail,
  getAppName,
  getToken,
};
