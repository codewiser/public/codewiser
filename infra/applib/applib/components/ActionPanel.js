import React, { Component, Fragment } from 'react';
import { Modal, Paper, Divider, Tooltip, Fab } from '@material-ui/core';
import { Menu, MenuItem } from '@material-ui/core';
import Zoom from '@material-ui/core/Zoom';
import MenuIcon from '@material-ui/icons/Menu';
import { withRouter } from 'react-router-dom';
import { ModalContainer } from './ModalContainer';
import {
	createMuiTheme,
	withStyles,
	ThemeProvider
} from '@material-ui/core/styles';

const MenuTheme = createMuiTheme({
	overrides: {
		MuiMenu: {
			paper: {
				background: 'rgba(58, 69, 130, 1)',
				color: '#fff'
			}
		},
		MuiMenuItem: {
			root: {
				fontFamily: 'CentraleSansRegular'
			}
		},
		MuiListItem: {
			button: {
				lineHeight: 2,
				fontFamily: 'CentraleSansRegular',
				'&:hover': {
					backgroundColor: 'rgba(115, 126, 191, 0.6)'
				}
			}
		}
	}
});

const styles = (theme) => ({
	fab: {
		margin: theme.spacing(2),
		cursor: 'pointer'
	},
	actionButtonStyle: {
		position: 'fixed',
		right: 0,
		zIndex: 100
	},
	paper: { width: '100%', maxWidth: '100%' }
});
class ActionPanelPure extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showComponent: null,
			setAnchorEl: null,
			anchorEl: null
		};
		this.handleClick = this.handleClick.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.handleClose = this.handleClose.bind(this);
		this.handleClickMenu = this.handleClickMenu.bind(this);
	}

	handleClose() {
		this.setState({ anchorEl: null });
	}

	handleClickMenu(event) {
		this.setState({
			anchorEl: event.currentTarget
		});
	}

	handleClick(data) {
		if (data.onClick instanceof Function) {
			data.onClick();
		} else {
			this.setState({ showComponent: data.onClick });
		}
	}

	closeModal() {
		this.setState({ showComponent: null });
	}

	render() {
		const { classes, fixed, width, height } = this.props;
		const { anchorEl } = this.state;
		const open = Boolean(anchorEl);
		return (
			<Fragment>
				{/* Hide if its in Iframe */}
				{!window.frameElement ? (
					<ThemeProvider theme={MenuTheme}>
						<div className={fixed ? classes.actionButtonStyle : ''}>
							<Tooltip
								style={{ cursor: 'pointer' }}
								TransitionComponent={Zoom}
								title="Menu"
								aria-label="add"
								onClick={this.handleClickMenu}
							>
								<Fab
									color="primary"
									style={{ cursor: 'pointer' }}
									className={classes.fab}
								>
									<MenuIcon />
								</Fab>
							</Tooltip>
							<Menu
								id="fade-menu"
								anchorEl={anchorEl}
								anchorOrigin={{
									vertical: 'bottom',
									horizontal: 'left'
								}}
								keepMounted
								open={open}
								onClose={this.handleClose}
								TransitionComponent={Zoom}
							>
								{this.props.actions.map((rowItem) =>
									rowItem.map((listItem, index) => (
										<MenuItem
											key={index}
											style={{ alignItems: 'left' }}
											onClick={(event) => this.handleClick(listItem)}
										>
											{listItem.label}
										</MenuItem>
									))
								)}
							</Menu>
						</div>
						{this.state.showComponent != null && (
							<div
								className={classes.root}
								style={{ width: '100%', maxWidth: '100%' }}
							>
								<ModalContainer
									classes={{
										paper: classes.paper
									}}
									style={{
										width: width ? width : '50%',
										height: height ? height : '90%',
										maxHeight: '90%',
										margin: 'auto'
									}}
									keepMounted
									title=""
									UIContiner={<Fragment>{this.state.showComponent}</Fragment>}
									open={this.state.showComponent != null}
									onClose={this.closeModal}
									isPrimaryButton={true}
								/>
							</div>
						)}
					</ThemeProvider>
				) : (
					''
				)}
			</Fragment>
		);
	}
}

const ActionPanel = withRouter(withStyles(styles)(ActionPanelPure));
export { ActionPanel };
