import React from 'react';
import ReactDOM from 'react-dom';
import { Header, graphqlClient } from '../applib';
import App from './components/App';
import { ApolloProvider } from 'react-apollo'
const appName = require('../package.json').name;

ReactDOM.render(
    <Header>
      <ApolloProvider client={graphqlClient(appName)}>
        <App/>
      </ApolloProvider>
    </Header>
  , document.getElementById('root-app')
);
