TAG=`cat version.txt`

export USER_ID=1000
export GROUP_ID=1000

TAG=$TAG$2
docker build --build-arg TAG=$TAG -t registry.gitlab.com/codewiser/public/codewiser/infra/schema:$TAG .
