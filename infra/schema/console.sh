echo 'Usage ./console.sh <hasura-cli project directory> server(optional)'
echo

IP_ADDRESS=127.0.0.1
PORT=9695
COMMAND='./hasura'

if [ -z $2 ]; then
  echo "‘DOMAIN_NAME’ variable is not set"
  END_POINT="http://platform.localhost/api"
else
  echo "‘DOMAIN_NAME’ variable is set to $DOMAIN_NAME"
  END_POINT="https://$2/api"
  if [ "$2" = "test" ]; then
    END_POINT="https://test.platform.codewiser.in/api"
    PORT=9694
  elif [ "$2" = "prod" ]; then
    END_POINT="https://platform.codewiser.in/api"
    PORT=9696
    PRODUCTION_VARS_FILE=./.production
    if [ -f "$PRODUCTION_VARS_FILE" ]; then
      source $PRODUCTION_VARS_FILE
      export $(cut -d= -f1 $PRODUCTION_VARS_FILE)
    else
      echo '.production file missing'
    fi
  else
    END_POINT="http://platform.localhost/api"
  fi
fi

echo 'Hasura CLI command = '$COMMAND
echo 'IP_ADDRESS = '$IP_ADDRESS
echo 'ACCESS_KEY = '$GRAPHQL_ACCESS_KEY
echo 'END_POINT = '$END_POINT
echo 'PORT = '$PORT

export HASURA_GRAPHQL_ENABLE_TELEMETRY=false
$COMMAND console --address $IP_ADDRESS --console-port $PORT --no-browser --admin-secret $GRAPHQL_ACCESS_KEY --endpoint $END_POINT --skip-update-check --project $1
