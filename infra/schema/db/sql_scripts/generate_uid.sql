CREATE OR REPLACE FUNCTION organisation.generate_uid(_organisation_id integer, _sequence text) RETURNS TEXT AS $$
DECLARE
_id integer;
_org_prefix text;
_prefix text;
_next_value integer;
_zero_pad integer;
_version text;
_value text;
_financial_year text;
_next_financial_year text;
_year text;
_month text;
_day text;
_next_year text;
BEGIN
UPDATE organisation.identifier SET next_value = next_value + 1
WHERE organisation_id = _organisation_id AND sequence = _sequence;

SELECT id, prefix, next_value::integer, zero_pad, version::text, financial_year::text
INTO _id, _prefix, _next_value, _zero_pad, _version, _financial_year
FROM organisation.identifier
WHERE organisation_id = _organisation_id AND sequence = _sequence;
_next_value := _next_value - 1;

SELECT prefix
INTO _org_prefix
FROM organisation.organisation
WHERE id = _organisation_id;

SELECT to_char(now(), 'YY'), to_char(now(), 'MM'), to_char(now(), 'DD')
INTO _year, _month, _day;

_value := _org_prefix || lpad(_version, 2, '0') || _prefix;

IF _sequence = 'invoice' THEN
    _next_year := rtrim(ltrim(to_char(to_number(_year, '99') + 1, '99')));
    IF _month = '04' AND _day = '01' THEN
        _next_financial_year := _year || _next_year;
        IF _financial_year != _next_financial_year THEN
            _financial_year := _next_financial_year;
            _next_value := 1;
        END IF;
    ELSE
        _financial_year := _year || _next_year;
    END IF;
    UPDATE organisation.identifier SET financial_year = _financial_year WHERE id = _id;
    _value := _value || _financial_year;
END IF;

IF _sequence = 'order' THEN
    _value := _value || _year || _month;
END IF;

IF _zero_pad IS NOT NULL THEN
    _value := _value || lpad(_next_value::text, _zero_pad, '0');
ELSE
    _value := _value || _next_value;
END IF;

--RAISE LOG 'called organisation.generate_uid (sequence: %)(uid: %)', _sequence,_value;

RETURN _value;
END;
$$ LANGUAGE plpgsql;
