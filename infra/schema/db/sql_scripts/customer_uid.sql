CREATE OR REPLACE FUNCTION customer.uid() RETURNS trigger AS $BODY$
DECLARE
_organisation_id integer;
BEGIN
    _organisation_id := NEW.organisation_id;
    NEW.cid := organisation.generate_uid(_organisation_id, 'customer');
    RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS customer_uid on customer.customer;
CREATE TRIGGER customer_uid BEFORE INSERT ON customer.customer
FOR EACH ROW EXECUTE PROCEDURE customer.uid();
