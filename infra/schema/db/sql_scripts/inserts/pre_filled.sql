DO $$
DECLARE
    _prefill integer:= 0;
BEGIN
SELECT COUNT(*) INTO _prefill FROM auth.user;
RAISE NOTICE 'PREFILL = %', _prefill;
IF _prefill = 0 THEN
    RAISE NOTICE 'INSERTING  DEFAULTS';
    -- insert users
    INSERT INTO auth.user (id, first_name, last_name, email, mobile, password_hash, user_status_id) VALUES(1, 'Admin', '', 'mounesh@codewiser.in', '0000000000', 'sha1$59f386ba$1$fda0e0c80d46920f704db9922b3311ac4671de8b', 1) ON CONFLICT (id) DO NOTHING;
    ALTER SEQUENCE auth.user_id_seq START WITH 2 MINVALUE 2 RESTART;

    -- insert service access
    INSERT INTO auth.service_access (id, user_id, service_id, role_id, created_by) VALUES(1, 1, 1, 1, 1) ON CONFLICT (id) DO NOTHING;
    INSERT INTO auth.service_access (id, user_id, service_id, role_id, created_by) VALUES(2, 1, 2, 1, 1) ON CONFLICT (id) DO NOTHING;
    ALTER SEQUENCE auth.service_access_id_seq START WITH 3 MINVALUE 3 RESTART;
ELSE
    RAISE NOTICE 'SKIPPING INSERT DEFAULTS';
END IF;
END $$;
