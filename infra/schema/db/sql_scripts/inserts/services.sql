-- insert Services
INSERT INTO auth.service (id, name, display_name) VALUES(1, 'home', 'Home') ON CONFLICT (id) DO UPDATE SET name=EXCLUDED.name, display_name=EXCLUDED.display_name;
INSERT INTO auth.service (id, name, display_name) VALUES(2, 'app', 'App1') ON CONFLICT (id) DO UPDATE SET name=EXCLUDED.name, display_name=EXCLUDED.display_name;
ALTER SEQUENCE auth.service_id_seq START WITH 3 MINVALUE 3 RESTART;
