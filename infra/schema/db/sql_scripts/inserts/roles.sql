-- insert roles
INSERT INTO auth.role (id, role, description) VALUES(1, 'admin', 'Super Admin') ON CONFLICT (id) DO NOTHING;
INSERT INTO auth.role (id, role, description) VALUES(2, 'user', 'User') ON CONFLICT (id) DO NOTHING;
ALTER SEQUENCE auth.role_id_seq START WITH 3 MINVALUE 3 RESTART;
