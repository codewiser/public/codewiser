-- insert Super Admin
INSERT INTO auth.user_status (id, name) VALUES(1, 'Active') ON CONFLICT (id) DO NOTHING;
INSERT INTO auth.user_status (id, name) VALUES(2, 'In Active') ON CONFLICT (id) DO NOTHING;
ALTER SEQUENCE auth.user_status_id_seq START WITH 3 MINVALUE 3 RESTART;
