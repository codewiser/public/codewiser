ROOT=/db

schemas=(
    'schema'
)

GRAPHQL_NOT_READY=true

while [ "$GRAPHQL_NOT_READY" = "true" ]
do
    url='http://graphql:8080/v1/version'
    status=$(curl --write-out %{http_code} --silent --output /dev/null ${url})

    if [ "$status" = "200" ]; then
        GRAPHQL_NOT_READY=false
    fi
    echo "Waiting for GraphQL server: GRAPHQL_NOT_READY=$GRAPHQL_NOT_READY, Curl Response: $status ..."
done

for dir in ${schemas[@]}
do
    echo 'Applying migrations for ' ${dir##*/}
    cd $ROOT/$dir/migrations    # print everything after the final "/"
    /hasura migrate apply --skip-update-check --admin-secret $GRAPHQL_ACCESS_KEY
done
psql postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@"postgres"/codewiser -b -f /db/sql_scripts/script.sql
