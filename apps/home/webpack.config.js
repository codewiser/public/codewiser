const HtmlWebPackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');
const package = require('./package.json')

module.exports = {
  entry: [
    'regenerator-runtime/runtime',
    './src/index.js'
  ],
  output: {
    path: __dirname + '/build/',
    publicPath: "/",
  },
  devServer: {
    publicPath: "/",
    compress: true,
    host: '0.0.0.0',
    disableHostCheck: true,
    historyApiFallback: {
      index: '/index.html'
    },
    port: 9000
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules\/(?!(@codewiser\/applib)\/).*/,
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: ["@babel/preset-env", "@babel/preset-react"]
            }
          }
        ]
      },     
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader"
          }
        ]
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin( {"process.env": {
        'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      }
    }),
    new HtmlWebPackPlugin({
      template: __dirname+"/index.html"
    }),
    new CopyWebpackPlugin([
      { from: 'static' }
    ])
  ]
};
