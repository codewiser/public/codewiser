// If this app running inside IFRAME likely in Aproval app To prevent following the link except Scrollbar
//************// var iframeElement = document.getElementsByTagName('iframe');
// var documents = iframeElement.iframe.contentDocument;
// documents.addEventListener('click', function(event) {}
//************* */
//*****  IF application runs inside IFRAME (window.frameElement) then prevent all Click events
// Allow only horizontal and verticle scrollbar */
if (window.frameElement) {
	document.addEventListener('click', function(event) {
		event.stopPropagation();
		event.stopImmediatePropagation();
		event.preventDefault(); //To prevent following the link
		console.log(
			'Prevent all Click events : Allow only horizontal and verticle scrollbar '
		);
	});
}
