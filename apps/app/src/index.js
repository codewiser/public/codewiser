import React from 'react';
import ReactDOM from 'react-dom';
import { Header, graphqlClient } from '@codewiser/applib';
import { ApolloProvider } from 'react-apollo'
const appName = require('../package.json').name;
import Routes from './Routes'

ReactDOM.render(
    <Header>
      <ApolloProvider client={graphqlClient(appName)}>
        <Routes/>
      </ApolloProvider>
    </Header>
  , document.getElementById('root-app')
);
