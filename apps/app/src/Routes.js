import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import packageJson from '../package.json';
import App from './components/App';
import Route1 from './components/Route1';

const Routes = () => {
  return (
    <BrowserRouter basename={packageJson.name}>
      <Route exact path="/" component={App} />
      <Route exact path="/route1" component={Route1} />
    </BrowserRouter>
  );
};

export default Routes;
